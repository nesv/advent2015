package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

var (
	unaryExp  = regexp.MustCompile(`(NOT) ([a-z0-9]+?) -> ([a-z0-9]+)\s`)
	binaryExp = regexp.MustCompile(`([a-z0-9]+?) (AND|RSHIFT|LSHIFT|OR) ([a-z0-9]+?) -> ([a-z0-9]+)\s`)
	simple    = regexp.MustCompile(`([a-z0-9]+?) -> ([a-z0-9]+)\s`)
)

func main() {
	input, err := ioutil.ReadFile("input7.txt")
	if err != nil {
		fmt.Println("error:", err)
		return
	}

	matches := make([][]string, 0)
	matches = append(matches, simple.FindAllStringSubmatch(string(input), -1)...)
	matches = append(matches, unaryExp.FindAllStringSubmatch(string(input), -1)...)
	matches = append(matches, binaryExp.FindAllStringSubmatch(string(input), -1)...)

	fmt.Println("7.1:", partOne(matches))

	registers = map[string]int{
		"b": partOne(matches),
	}
	fmt.Println(partOne(matches))
}

var registers = make(map[string]int)

func isInt(s string) (int, bool) {
	v, err := strconv.ParseInt(s, 10, 16)
	return int(v), err == nil
}

func set(reg string, v int) {
	registers[reg] = v
	fmt.Printf("%s=%d\n", reg, v)
}

// Holds a pointer to which operation sets the register.
var regop = make(map[string]int)

func partOne(ops [][]string) int {
	for i, op := range ops {
		regop[op[len(op)-1]] = i
	}

	//fmt.Println("a is set by operation", regop["a"])
	return evaluate("a", ops)
}

func evaluate(reg string, ops [][]string) int {
	if v, ok := registers[reg]; ok {
		return v
	}

	op := ops[regop[reg]]
	fmt.Println("jmp", reg, "=", strings.TrimSpace(op[0]))

	// Simple assignments
	if v, ok := isInt(op[1]); ok && len(op) == 3 {
		set(op[2], v)
		return v
	} else if !ok && len(op) == 3 {
		v := evaluate(op[1], ops)
		set(op[2], v)
		return v
	}

	// NOT x -> y
	if op[1] == "NOT" {
		return ^evaluate(op[2], ops)
	}

	// Other operations.
	var lv, rv, result int
	var ok bool
	var dest string = op[len(op)-1]

	if lv, ok = isInt(op[1]); !ok {
		lv = evaluate(op[1], ops)
	}
	if rv, ok = isInt(op[3]); !ok {
		rv = evaluate(op[3], ops)
	}

	fmt.Printf("%s %s %s, %s=%d %s=%d\n", op[1], op[2], op[3], op[1], lv, op[3], rv)

	switch op[2] {
	case "AND":
		result = lv & rv
	case "OR":
		result = lv | rv
	case "LSHIFT":
		result = lv << uint(rv)
	case "RSHIFT":
		result = lv >> uint(rv)
	}

	set(dest, result)
	return result
}
