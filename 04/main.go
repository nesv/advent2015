package main

import (
	"crypto/md5"
	"fmt"
	"strings"
)

const Input string = "yzbqklnj"

func main() {
	var i int = 0
	var a bool = false
	for {
		data := []byte(fmt.Sprintf("%s%d", Input, i))
		sum := fmt.Sprintf("%x", md5.Sum(data))
		if strings.HasPrefix(sum, "00000") && !a {
			fmt.Printf("4.1: %d\n", i)
			a = true
		} else if strings.HasPrefix(sum, "000000") {
			fmt.Printf("4.2: %d\n", i)
			break
		}
		i++
	}
}
