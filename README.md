# Advent of Code 2015

My solutions for the [Advent of Code](http://adventofcode.com/day/8) 2015 code
challenge.
