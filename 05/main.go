package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func loadData() ([]string, error) {
	f, err := os.Open("input5.txt")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	strs := make([]string, 0)
	for {
		var s string
		_, err := fmt.Fscanf(f, "%s\n", &s)
		if err != nil && err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
		strs = append(strs, s)
	}

	return strs, nil
}

func main() {
	strs, err := loadData()
	if err != nil {
		fmt.Println("error:", err)
		return
	}

	var nice int
	var nice2 int
	for _, s := range strs {
		if testOne(s) && testForVowels(s) && testForDoubles(s) {
			nice++
		}

		if testTwo(s) {
			nice2++
		}
	}

	fmt.Println("5.1:", nice)
	fmt.Println("5.2:", nice2)
}

func testOne(s string) bool {
	for _, substr := range []string{"ab", "cd", "pq", "xy"} {
		if strings.Contains(s, substr) {
			return false
		}
	}
	return true
}

func testForVowels(s string) bool {
	numVowels := 0
	for _, c := range s {
		switch c {
		case 'a', 'e', 'i', 'o', 'u':
			numVowels++
		}
	}
	return numVowels >= 3
}

func testForDoubles(s string) bool {
	for i := 0; i < len(s)-1; i++ {
		if s[i] == s[i+1] {
			return true
		}
	}
	return false
}

func testTwo(s string) bool {
	valid := testTwoA(s) && testTwoB(s)
	if valid {
		fmt.Printf("debug:5.2:s=%q\n", s)
	}
	return valid
}

func testTwoA(s string) bool {
	for i := 0; i < len(s)-1; i++ {
		if strings.Contains(s[i+2:], s[i:i+2]) {
			return true
		}
	}
	return false
}

func testTwoB(s string) bool {
	for i := 0; i < len(s)-2; i++ {
		if s[i] == s[i+2] {
			fmt.Printf("debug:5.2.2:s=%q seq=%q i=%d\n", s, s[i:i+3], i)
			return true
		}
	}
	return false
}
