package main

import (
	"fmt"
	"io"
	"os"
	"sort"
)

func main() {
	data, err := gatherData()
	if err != nil {
		fmt.Println("error:", err)
		return
	}

	var wrappingPaper, ribbon int
	for _, present := range data {
		wrappingPaper += present.WrappingPaper()
		ribbon += present.Ribbon()
	}
	fmt.Printf("2.1: %d\n2.2: %d\n", wrappingPaper, ribbon)
}

type Present struct {
	L, H, W int
}

func (p Present) WrappingPaper() int {
	a := p.L * p.W
	b := p.W * p.H
	c := p.H * p.L

	dims := []int{a, b, c}
	sort.Ints(dims)

	return dims[0] + 2*a + 2*b + 2*c
}

func (p Present) Ribbon() int {
	dims := []int{p.L, p.H, p.W}
	sort.Ints(dims)

	return dims[0] + dims[0] + dims[1] + dims[1] + (p.L * p.W * p.H)
}

func gatherData() ([]Present, error) {
	data := make([]Present, 0)

	inputFile, err := os.Open("input2.txt")
	if err != nil {
		return nil, err
	}
	defer inputFile.Close()

	for {
		var l, h, w int
		if _, err := fmt.Fscanf(inputFile, "%dx%dx%d\n", &l, &h, &w); err != nil && err == io.EOF {
			return data, nil
		} else if err != nil {
			return data, err
		}
		data = append(data, Present{L: l, H: h, W: w})
	}

	return data, nil
}
