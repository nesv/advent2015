package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type Instruction struct {
	Command        string
	StartX, StartY int
	EndX, EndY     int
}

var re = regexp.MustCompile(`(turn on|turn off|toggle) ([0-9]{1,3}),([0-9]{1,3}) through ([0-9]{1,3}),([0-9]{1,3})`)

func loadInstructions() ([]Instruction, error) {
	p, err := ioutil.ReadFile("input6.txt")
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(p), "\n")
	instructions := make([]Instruction, len(lines))
	for i, line := range lines {
		matches := re.FindAllStringSubmatch(line, -1)
		if len(matches) == 0 {
			return instructions, nil
		}
		if len(matches[0]) < 6 {
			return nil, fmt.Errorf("too few matches in line")
		}

		var sx, sy, ex, ey int

		if v, err := strconv.Atoi(matches[0][2]); err != nil {
			return nil, err
		} else {
			sx = v
		}
		if v, err := strconv.Atoi(matches[0][3]); err != nil {
			return nil, err
		} else {
			sy = v
		}
		if v, err := strconv.Atoi(matches[0][4]); err != nil {
			return nil, err
		} else {
			ex = v
		}
		if v, err := strconv.Atoi(matches[0][5]); err != nil {
			return nil, err
		} else {
			ey = v
		}

		instructions[i] = Instruction{
			Command: matches[0][1],
			StartX:  sx,
			StartY:  sy,
			EndX:    ex,
			EndY:    ey,
		}
	}

	return instructions, nil
}

func main() {
	instructions, err := loadInstructions()
	if err != nil {
		fmt.Println("error:", err)
		return
	}

	lights := make([][]int, 1000)
	for i := 0; i < 1000; i++ {
		lights[i] = make([]int, 1000)
	}

	var brightness int
	for _, inst := range instructions {
		for i := inst.StartX; i <= inst.EndX; i++ {
			for j := inst.StartY; j <= inst.EndY; j++ {
				switch inst.Command {
				case "turn on":
					lights[i][j]++
					brightness++
				case "turn off":
					if lights[i][j] > 0 {
						lights[i][j]--
						brightness--
					}
				case "toggle":
					lights[i][j] += 2
					brightness += 2
				}
			}
		}
	}

	fmt.Println("6.2:", brightness)
}
